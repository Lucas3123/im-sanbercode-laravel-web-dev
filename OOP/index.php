<?php
require_once("animal.php");
require_once("frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");

echo "Name :" . $sheep->name . "<br>"; // "shaun"
echo "legs :" . $sheep->legs . "<br>"; // 4
echo "cold_blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold_blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump("Hop Hop") . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong ->name . "<br>";
echo "legs : " . $sungokong ->legs . "<br>";
echo "cold_blooded : " . $sungokong ->cold_blooded . "<br>";
echo  $sungokong->yell("Auoo"). "<br> <br>"; // "Auooo"

?>