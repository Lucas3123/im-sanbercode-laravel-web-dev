@extends('layout.master')
@section('title')
   <h1>Buat account Baru</h1>
@endsection 
@section('subtitle')

<h3>Sign Up Form</h3>

@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label>
        <input type="text" name="nama"><br><br>
        <label>Last name :</label>
        <input type="text" name="last"><br><br>
        <label>Gender :</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br>

        <label>Nationality</label>
        <select name = "Nationality"><br>
            <option value ="indonesian">Indonesian</option>
            <option value ="Singaporean">Singaporean</option>
            <option value ="Malayasian">Malayasian</option>
            <option value ="Australian">Australian</option>

        </select> <br><br>
        <label>Language Spoken :</label><br>
        <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="Language Spoken">English<br>
        <input type="checkbox" name="Language Spoken">Other<br>

        <textarea name="message" rows="10" cols="30"></textarea> <br><br>
        <input type="submit" value="    Sign up">
    </form>
@endsection